# Getting Started
This is a small Node.js KoA REST server example. It has no function other than to express concepts for building REST interfaces within Node.js micro-services.

To run:
- ```npm i```
- ```npm run start```

## Routes
### ```/health GET``` Returns the current up status of the service
### ```/showtrace GET``` Returns a JSON object containing the generated trace id for that transaction.
### ```/internalapicall GET``` Makes an API call to /showtrace with an existing x-trace-id header that should be logged and used for all traceids for the transaction. 
### ```/externalapicall GET``` Makes an API call to https://catfact.ninja/fact with a generated x-trace-id header and returns the result with a head containing the same trace id.