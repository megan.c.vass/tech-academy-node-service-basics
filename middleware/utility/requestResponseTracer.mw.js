const crypto = require('crypto');
const { constants: { TRACE_HEADER_KEY } } = require('../../util/')

module.exports = async (ctx, next) => {
    const {log, traceId, response: {statusCode} } = ctx;

    // Log our response status and trace id so we can see what the service sent back
    log.info({
        traceId,
        statusCode
    });

    return next();
};