const crypto = require('crypto');
const { constants: { TRACE_HEADER_KEY } } = require('../../util/')

module.exports = async (ctx, next) => {
    console.log(ctx.request.headers)
    // If no trace id header is found add one to our request 
    // response and request context
    if ( !ctx.request.headers[TRACE_HEADER_KEY] ) {
        // Create a hash out of current time that will id this transaction.
        const id = `${crypto
            .createHash('md5')
            .update(Date.now().toString())
            .digest('hex')
            .toString()}-trace`;
        // Assign this trace id to a property available to all cascading middleware.
        ctx.traceId = id;
        // Set the id within the response headers 
        ctx.response.set(TRACE_HEADER_KEY, id);
    } else {
        const traceId = ctx.request.headers[TRACE_HEADER_KEY];

        ctx.log.info(`Fowarding found trace id ${traceId}`);
        ctx.traceId = traceId;
        ctx.response.set(TRACE_HEADER_KEY, traceId);
    }

    return next();
};