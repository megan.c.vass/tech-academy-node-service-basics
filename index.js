// HTTP Framework
const Koa = require('koa');
// Allows simple route management via the Koa engine
const Router = require('@koa/router');
// Adds support for JSON parsing in HTTP request bodies.
const { bodyParser } = require("@koa/bodyparser");
// Handles the cross domain issues we would hit making 
// localhost calls to an external API
const cors = require('@koa/cors');

const pino = require('pino');
const transport = pino.transport({
    targets: [
      {
        level: 'trace',
        target: 'pino/file',
        options: {
          destination: `${__dirname}/logs/service.log`,
        },
      },
      {
        level: 'trace',
        target: 'pino-pretty',
        options: {},
      },
    ],
  });

// init our logger instance to integrate with Koa
const koaPino = require('koa-pino-logger');

// pull in our custom middleware to be added to the cascade.
const { requestTracer } = require('./middleware');

// import our routes so they can be added to the app.
const routes = require('./routes');

const app = new Koa();
var router = new Router();

app.use(
    koaPino({}, transport)
);
app.use(cors())
app.use(bodyParser());
app.use(requestTracer);

// Load in all routes that are defined in /route/index.js
Object.values(routes).forEach(route => {
    console.log(route)
    router[route.method](route.path, route.handler);
});
app.use(router.routes());

app.listen(3000);
