const routeHandler = async (ctx, next) => {
    ctx.res.statusCode = 200;
    ctx.body = { traceId: ctx.traceId };
};

module.exports = {
    handler: routeHandler,
    path: '/templateroute',
    method: 'get'
};