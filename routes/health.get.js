const healthHandler = async (ctx, next) => {
    ctx.res.statusCode = 200;
    ctx.body = { status: 'I\'m alive!' };
};

module.exports = {
    handler: healthHandler,
    path: '/health',
    method: 'get'
};