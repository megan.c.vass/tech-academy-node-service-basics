const fetch = require('node-fetch');
const { constants: { TRACE_HEADER_KEY } } = require('../util');

const internalApiHandler = async (ctx, next) => {
    try {
        const response = await fetch(
            'http://localhost:3000/showTrace',
            {
                headers: {
                    [TRACE_HEADER_KEY]: ctx.traceId
                }
            }
        ).then(res => res.json());
        
        ctx.response.statusCode = 200;
        ctx.body = { response };
    } catch (externalError) {
        console.log(externalError)
        ctx.log.error( {
            error: JSON.stringify(externalError),
        });
    }
};
 
module.exports = {
    handler: internalApiHandler,
    path: '/internalapicall',
    method: 'get'
};