const fetch = require('node-fetch');
const { constants: { TRACE_HEADER_KEY } } = require('../util');

const externalApiHandler = async (ctx, next) => {
    try {
        ctx.log.info(`Found trace id ${ctx.traceId}, sending with vendor call`);
        const response = await fetch(
            'https://catfact.ninja/fact', 
            {
                headers: {
                    [TRACE_HEADER_KEY]: ctx.traceId
                }
            }
        ).then(res => res.json());
        
        ctx.response.statusCode = 200;
        ctx.body = { response };
    } catch (externalError) {
        console.log(externalError)
        ctx.log.error( {
            error: JSON.stringify(externalError),
        });
    }

};
 
module.exports = {
    handler: externalApiHandler,
    path: '/externalapicall',
    method: 'get'
};