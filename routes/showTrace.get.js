const showTraceHandler = async (ctx, next) => {
    ctx.res.statusCode = 200;
    ctx.body = { traceId: ctx.traceId};
};

module.exports = {
    handler: showTraceHandler,
    path: '/showtrace',
    method: 'get'
};