module.exports = {
    health: require('./health.get'),
    showTraceGet: require('./showTrace.get'),
    externalApiGet: require('./externalApi.get'),
    internalApiGet: require('./internalApi.get')
};